import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './demos/home/home.component';
import {NgModule} from '@angular/core';
import {AccountComponent} from './demos/account/account.component';
import {TransactionComponent} from './demos/transaction/transaction.component';
import {PersonalInfoComponent} from './demos/personal-info/personal-info.component';
import {LoginComponent} from './demos/auth/login/login.component';
import {RegisterComponent} from './demos/auth/register/register.component';
import {AppGuard} from './app.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    canActivate: [AppGuard],
    children: [
      {
        path: 'ho',
        component: HomeComponent
      },
      {
        path: 'ac',
        component: AccountComponent
      },
      {
        path: 'tr',
        component: TransactionComponent
      },
      {
        path: 'pe',
        component: PersonalInfoComponent
      },
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
