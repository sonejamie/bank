import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './demos/auth/login/login.component';
import { HomeComponent } from './demos/home/home.component';
import { AccountComponent } from './demos/account/account.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { TransactionComponent } from './demos/transaction/transaction.component';
import { PrimarytransactionComponent } from './demos/transaction/primarytransaction/primarytransaction.component';
import { SavingstransactionComponent } from './demos/transaction/savingstransaction/savingstransaction.component';
import { UpdatePrimarytransactionComponent } from './demos/transaction/primarytransaction/update-primarytransaction/update-primarytransaction.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UpdateSavingstransactionComponent } from './demos/transaction/savingstransaction/update-savingstransaction/update-savingstransaction.component';
import { PersonalInfoComponent } from './demos/personal-info/personal-info.component';
import { UpdatePersonalInfoComponent } from './demos/personal-info/update-personal-info/update-personal-info.component';
import { LogoutComponent } from './demos/auth/logout/logout.component';
import {AppGuard} from './app.guard';
import {RegisterComponent} from './demos/auth/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AccountComponent,
    TransactionComponent,
    PrimarytransactionComponent,
    SavingstransactionComponent,
    UpdatePrimarytransactionComponent,
    UpdateSavingstransactionComponent,
    PersonalInfoComponent,
    UpdatePersonalInfoComponent,
    LogoutComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [AppGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
