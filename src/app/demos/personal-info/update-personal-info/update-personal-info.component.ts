import { Component, OnInit } from '@angular/core';
import {PersonalInfo} from '../../models/Personal-Info';
import {PersonalInfoService} from '../../service/personal-info.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PrimaryTransaction} from '../../models/PrimaryTransaction';

@Component({
  selector: 'app-update-personal-info',
  templateUrl: './update-personal-info.component.html',
  styleUrls: ['./update-personal-info.component.css']
})
export class UpdatePersonalInfoComponent implements OnInit {

  id: number;
  personalinfo: PersonalInfo;

  constructor(private personalinfoSerivce: PersonalInfoService,
              private router: Router,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {

    this.personalinfo = new PersonalInfo();

    this.id = this.route.snapshot.params.id;

    this.personalinfoSerivce.getpersonalinfo(this.id)
      .subscribe(data => {
        console.log(data);
        this.personalinfo = data;
      }, error => console.log(error));
  }

  updatePersonalInfo() {
    this.personalinfoSerivce.updatePersonalInfo(this.id, this.personalinfo)
      .subscribe(data => {
          console.log(data);
          this.personalinfo = new PersonalInfo ();
        },
        error => console.log(error));
  }

  onSubmit() {
    this.updatePersonalInfo();
  }


}
