import { Component, OnInit } from '@angular/core';
import {PersonalInfo} from '../models/Personal-Info';
import {PersonalInfoService} from '../service/personal-info.service';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.css']
})
export class PersonalInfoComponent implements OnInit {

  personalinfos: PersonalInfo[];

  personalinfo: PersonalInfo = new PersonalInfo();

  id: number;

  constructor(private personalinfoService: PersonalInfoService) { }

  ngOnInit(): void {
    this.personalinfoService.GetPersonalInfo().subscribe(res => {
      this.personalinfos = res;
    });
  }

  addPersonalInfo(): void{
    this.personalinfoService.addPersonalInfo(this.personalinfo)
      .subscribe(data => {
        alert('PersonalInfo created successfully.');
      });
  }

  deletePersonalInfo(personalinfo: PersonalInfo): void{
    this.personalinfoService.deletePersonalInfo(personalinfo)
      .subscribe( data => {
        this.personalinfos = this.personalinfos.filter(p => p !== personalinfo);
      });
  }

}
