export class PrimaryTransaction {
  id: number;
  date: any;
  description: string;
  type: string;
  status: string;
  amount: number;
}
