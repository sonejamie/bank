export class PersonalInfo{
  id: number;
  name: string;
  DOB: any;
  streetaddress: string;
  city: string;
  state: string;
  zipcode: string;
  salary: number;
  educationlevel: string;
  fico: number;
  mortgage: number;


}
