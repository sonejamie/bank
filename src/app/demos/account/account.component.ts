import { Component, OnInit } from '@angular/core';
import {PrimaryAccount} from '../models/PrimaryAccount';
import {PrimaryaccountService} from '../service/primaryaccount.service';
import {SavingsaccountService} from '../service/savingsaccount.service';
import {SavingsAccount} from '../models/SavingsAccount';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  primaryaccounts: PrimaryAccount[];

  primaryaccount: PrimaryAccount = new PrimaryAccount();

  savingsaccounts: SavingsAccount[];

  savingsaccount: SavingsAccount = new SavingsAccount();

  id: number;



  constructor(private primaryaccountService: PrimaryaccountService,
              private savingsaccountService: SavingsaccountService) {

  }

  ngOnInit(): void {
    this.primaryaccountService.GetPrimaryAccount().subscribe(res => {
      this.primaryaccounts = res;
    });
    this.savingsaccountService.GetSavingsAccount().subscribe(res => {
      this.savingsaccounts = res;
    });
  }

  addPrimaryAccount(): void{
    this.primaryaccountService.addPrimaryAccount(this.primaryaccount)
      .subscribe(data => {
        alert('PrimaryAccount created successfully.');
      });
  }

  deletePrimaryAccount(primaryaccount: PrimaryAccount): void {
    this.primaryaccountService.deletePrimaryAccount(primaryaccount)
      .subscribe(data => {
        this.primaryaccounts = this.primaryaccounts.filter(p => p !== primaryaccount);
      });
  }

  addSavingsAccount(): void{
    this.savingsaccountService.addSavingsAccount(this.savingsaccount)
      .subscribe(data => {
        alert('SavingsAccount created successfully.');
      });
  }

  deleteSavingsAccount(savingsaccount: SavingsAccount): void {
    this.savingsaccountService.deleteSavingsAccount(savingsaccount)
      .subscribe(data => {
        this.savingsaccounts = this.savingsaccounts.filter(s => s !== savingsaccount);
      });
  }



}
