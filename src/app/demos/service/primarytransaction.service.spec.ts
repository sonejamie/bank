import { TestBed } from '@angular/core/testing';

import { PrimarytransactionService } from './primarytransaction.service';

describe('PrimarytransactionService', () => {
  let service: PrimarytransactionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrimarytransactionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
