import { Injectable } from '@angular/core';
import {SavingsAccount} from '../models/SavingsAccount';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SavingsaccountService {

  savingsaccount: SavingsAccount = new SavingsAccount();

  private API_URL = `${environment.API_URL}`;
  private baseUrl = 'http://localhost:8081/savingsaccount';

  constructor(private http: HttpClient) {

  }

  public GetSavingsAccount(): Observable<any> {
    return this.http.get<SavingsAccount[]>(this.API_URL + '/savingsaccount', {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  public getsavingsaccount(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  public addSavingsAccount(savingsaccount: SavingsAccount): Observable<any> {
    return this.http.post<SavingsAccount>(this.API_URL + 'savingsaccount', savingsaccount);
  }

  public deleteSavingsAccount(savingsaccount: SavingsAccount): Observable<any> {
    return this.http.delete(this.API_URL + '/savingsaccount/' + savingsaccount.id);
  }





}
