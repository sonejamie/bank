import { Injectable } from '@angular/core';
import {SavingsTransaction} from '../models/SavingsTransaction';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {PrimaryTransaction} from '../models/PrimaryTransaction';

@Injectable({
  providedIn: 'root'
})
export class SavingstransactionService {

  savingstransaction: SavingsTransaction = new SavingsTransaction();

  private API_URL = `${environment.API_URL}`;
  private baseUrl = 'http://localhost:8081/savingstransaction';

  constructor(private http: HttpClient) {

  }

  public GetSavingsTransaction(): Observable<any> {
    return this.http.get<SavingsTransaction[]>(this.API_URL + '/savingstransaction', {withCredentials: true} )
      .pipe(tap((res) => {
        return res;
      }));
  }

  public getsavingstransaction(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  public addSavingsTransaction(savingstransaction: SavingsTransaction): Observable<any> {
    return this.http.post<SavingsTransaction>(this.API_URL + '/savingstransaction', savingstransaction);
  }

  public deleteSavingsTransaction(savingstransaction: SavingsTransaction): Observable<any> {
    return this.http.delete(this.API_URL + '/savingstransaction/' + savingstransaction.id );
  }

  public updateSavingsTransaction(id: any, savingstransaction: SavingsTransaction): Observable<any> {
    return this.http.put(this.API_URL + '/savingstransaction/edit/' + savingstransaction.id, savingstransaction);
  }




}
