import { TestBed } from '@angular/core/testing';

import { PrimaryaccountService } from './primaryaccount.service';

describe('PrimaryaccountService', () => {
  let service: PrimaryaccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrimaryaccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
