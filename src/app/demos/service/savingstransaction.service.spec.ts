import { TestBed } from '@angular/core/testing';

import { SavingstransactionService } from './savingstransaction.service';

describe('SavingstransactionService', () => {
  let service: SavingstransactionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SavingstransactionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
