import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private API_URL = `${environment.API_URL}`;

  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private router: Router) { }

  login(user): Observable<any> {
    const httpParams: HttpParams = new HttpParams()
      .append('username', user.username)
      .append('password', user.password);
    return this.http.post(this.API_URL + '/login', httpParams, {withCredentials: true})
      .pipe(tap((res) => {
        this.loggedIn.next(res.success);
        if (this.loggedIn) {
          this.router.navigate(['/ho']);
        }
        return res;
      }));
  }

  checklogin(): Observable<any> {
    return this.http.get(this.API_URL + '/checklogin', {withCredentials: true})
      .pipe(tap((res) => {
        this.loggedIn.next(res.success);
        return res;
      }));
  }

  logout(): Observable<any> {
    return this.http.post(this.API_URL + '/logout', {}, {withCredentials: true})
      .pipe(tap((res) => {
        this.loggedIn.next(false);
        this.router.navigate(['/login']);
        return res;
      }));
  }

  register(user): Observable<any> {
    return this.http.post(this.API_URL + '/users', user)
      .pipe(tap(res => {
        console.log(res);
        if (res.json().success) {
          this.router.navigate(['/login']);
        }
      }));
  }

}
