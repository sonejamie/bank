import { Injectable } from '@angular/core';
import {PersonalInfo} from '../models/Personal-Info';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonalInfoService {

  personalinfo: PersonalInfo = new PersonalInfo();

  private API_URL = `${environment.API_URL}`;
  private baseUrl = 'http://localhost:8081/personal-info';

  constructor(private http: HttpClient) {

  }

  public GetPersonalInfo(): Observable<any>{
    return this.http.get<PersonalInfo[]>(this.API_URL + '/personal-info', {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  public getpersonalinfo(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  public addPersonalInfo(personalinfo: PersonalInfo): Observable<any> {
    return this.http.post<PersonalInfo>(this.API_URL + '/personal-info', personalinfo);
  }

  public deletePersonalInfo(personalinfo: PersonalInfo): Observable<any> {
    return this.http.delete(this.API_URL + '/personal-info/' + personalinfo);
  }

  public updatePersonalInfo(id: any, personalinfo: PersonalInfo): Observable<any>{
    return this.http.put(this.API_URL + '/personal-info/edit/' +  personalinfo.id, personalinfo);
  }




}
