import { TestBed } from '@angular/core/testing';

import { SavingsaccountService } from './savingsaccount.service';

describe('SavingsaccountService', () => {
  let service: SavingsaccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SavingsaccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
