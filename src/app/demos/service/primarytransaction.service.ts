import { Injectable } from '@angular/core';
import {PrimaryTransaction} from '../models/PrimaryTransaction';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class PrimarytransactionService {

  primarytransaction: PrimaryTransaction = new PrimaryTransaction();

  private API_URL = `${environment.API_URL}`;
  private baseUrl = 'http://localhost:8081/primarytransaction';

  constructor(private http: HttpClient) {

  }

  public GetPrimaryTransaction(): Observable<any> {
    return this.http.get<PrimaryTransaction[]>(this.API_URL + '/primarytransaction', {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  public getprimarytransaction(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  public addPrimaryTransaction(primarytransaction: PrimaryTransaction): Observable<any> {
    return this.http.post<PrimaryTransaction>(this.API_URL + '/primarytransaction', primarytransaction);
  }

  public deletePrimaryTransaction(primarytransaction: PrimaryTransaction): Observable<any> {
    return this.http.delete(this.API_URL + '/primarytransaction/' + primarytransaction.id );
  }

  public updatePrimaryTransaction(id: any, primarytransaction: PrimaryTransaction): Observable<any> {
    return this.http.put(this.API_URL + '/primarytransaction/edit/' + primarytransaction.id, primarytransaction);
  }

}
