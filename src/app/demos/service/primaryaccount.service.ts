import { Injectable } from '@angular/core';
import {PrimaryAccount} from '../models/PrimaryAccount';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PrimaryaccountService {

  primaryaccount: PrimaryAccount = new PrimaryAccount();

  private API_URL = `${environment.API_URL}`;
  private baseUrl = 'http://localhost:8081/primaryaccount';

  constructor(private http: HttpClient) {

  }

  public GetPrimaryAccount(): Observable<any> {
    return this.http.get<PrimaryAccount[]>(this.API_URL + '/primaryaccount', {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  public getprimaryaccount(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  public addPrimaryAccount(primaryaccount: PrimaryAccount): Observable<any> {
    return this.http.post<PrimaryAccount>(this.API_URL + '/primaryaccount', primaryaccount);
  }

  public deletePrimaryAccount(primaryaccount: PrimaryAccount): Observable<any>{
    return this.http.delete(this.API_URL + '/primaryaccount/' + primaryaccount.id);
  }


}
