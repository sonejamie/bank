import { Component, OnInit } from '@angular/core';
import {PrimaryTransaction} from '../../models/PrimaryTransaction';
import {PrimarytransactionService} from '../../service/primarytransaction.service';

@Component({
  selector: 'app-primarytransaction',
  templateUrl: './primarytransaction.component.html',
  styleUrls: ['./primarytransaction.component.css']
})
export class PrimarytransactionComponent implements OnInit {

  primarytransactions: PrimaryTransaction[];

  primarytransaction: PrimaryTransaction = new PrimaryTransaction();

  id: number;

  constructor(private primarytransactionService: PrimarytransactionService) {

  }

  ngOnInit(): void {
    this.primarytransactionService.GetPrimaryTransaction().subscribe(res => {
    this.primarytransactions = res;
    });
  }

  addPrimaryTransaction(): void{
    this.primarytransactionService.addPrimaryTransaction(this.primarytransaction)
      .subscribe(data => {
        alert('PrimaryTransaction created successfully.');
      });
  }

  deletePrimaryTransaction(primarytransaction: PrimaryTransaction): void {
    this.primarytransactionService.deletePrimaryTransaction(primarytransaction)
      .subscribe( data => {
        this.primarytransactions = this.primarytransactions.filter(pt => pt !== primarytransaction);
    });
  }





}
