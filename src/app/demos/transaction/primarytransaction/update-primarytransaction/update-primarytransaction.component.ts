import { Component, OnInit } from '@angular/core';
import {PrimaryTransaction} from '../../../models/PrimaryTransaction';
import {PrimarytransactionService} from '../../../service/primarytransaction.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-update-primarytransaction',
  templateUrl: './update-primarytransaction.component.html',
  styleUrls: ['./update-primarytransaction.component.css']
})
export class UpdatePrimarytransactionComponent implements OnInit {

  id: number;
  primarytransaction: PrimaryTransaction;

  constructor(private primarytransactionService: PrimarytransactionService,
              private router: Router,
              private route: ActivatedRoute) {

  }

  ngOnInit() {

    this.primarytransaction = new PrimaryTransaction();

    this.id = this.route.snapshot.params.id;

    this.primarytransactionService.getprimarytransaction(this.id)
      .subscribe(data => {
        console.log(data);
        this.primarytransaction = data;
      }, error => console.log(error));
  }

  updatePrimaryTransaction() {
    this.primarytransactionService.updatePrimaryTransaction(this.id, this.primarytransaction)
      .subscribe(data => {
          console.log(data);
          this.primarytransaction = new PrimaryTransaction();
        },
        error => console.log(error));
  }

  onSubmit() {
    this.updatePrimaryTransaction();
  }

}
