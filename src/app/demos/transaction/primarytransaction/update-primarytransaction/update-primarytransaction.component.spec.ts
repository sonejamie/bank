import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePrimarytransactionComponent } from './update-primarytransaction.component';

describe('UpdatePrimarytransactionComponent', () => {
  let component: UpdatePrimarytransactionComponent;
  let fixture: ComponentFixture<UpdatePrimarytransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatePrimarytransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePrimarytransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
