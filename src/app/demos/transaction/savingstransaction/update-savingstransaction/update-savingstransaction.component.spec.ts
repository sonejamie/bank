import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSavingstransactionComponent } from './update-savingstransaction.component';

describe('UpdateSavingstransactionComponent', () => {
  let component: UpdateSavingstransactionComponent;
  let fixture: ComponentFixture<UpdateSavingstransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateSavingstransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSavingstransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
