import { Component, OnInit } from '@angular/core';
import {PrimarytransactionService} from '../../../service/primarytransaction.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SavingstransactionService} from '../../../service/savingstransaction.service';
import {SavingsTransaction} from '../../../models/SavingsTransaction';
import {PrimaryTransaction} from '../../../models/PrimaryTransaction';

@Component({
  selector: 'app-update-savingstransaction',
  templateUrl: './update-savingstransaction.component.html',
  styleUrls: ['./update-savingstransaction.component.css']
})
export class UpdateSavingstransactionComponent implements OnInit {

  id: number;
  savingstransaction: SavingsTransaction;

  constructor(private savingstransactionService: SavingstransactionService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.savingstransaction = new SavingsTransaction();

    this.id = this.route.snapshot.params.id;

    this.savingstransactionService.getsavingstransaction(this.id)
      .subscribe(data => {
        console.log(data);
        this.savingstransaction = data;
      }, error => console.log(error));
  }

  updateSavingsTransaction(){
    this.savingstransactionService.updateSavingsTransaction(this.id, this.savingstransaction )
      .subscribe(data => {
          console.log(data);
          this.savingstransaction = new SavingsTransaction();
        },
        error => console.log(error));
  }

  onSubmit(){
    this.updateSavingsTransaction()
  }




}
