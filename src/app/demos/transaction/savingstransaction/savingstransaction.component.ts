import { Component, OnInit } from '@angular/core';
import {SavingsTransaction} from '../../models/SavingsTransaction';
import {SavingstransactionService} from '../../service/savingstransaction.service';
import {PrimaryTransaction} from '../../models/PrimaryTransaction';

@Component({
  selector: 'app-savingstransaction',
  templateUrl: './savingstransaction.component.html',
  styleUrls: ['./savingstransaction.component.css']
})
export class SavingstransactionComponent implements OnInit {

  savingstransactions: SavingsTransaction[];

  savingstransaction: SavingsTransaction = new SavingsTransaction();

  id: number;

  constructor(private savingstransactionService: SavingstransactionService) {

  }

  ngOnInit(): void {
    this.savingstransactionService.GetSavingsTransaction().subscribe(res => {
      this.savingstransactions = res;
    });
  }

  addSavingsTransaction(): void{
    this.savingstransactionService.addSavingsTransaction(this.savingstransaction)
      .subscribe(data => {
        alert('SavingsTransaction created successfully.');
      });
  }

  deleteSavingsTransaction(savingstransaction: SavingsTransaction): void {
    this.savingstransactionService.deleteSavingsTransaction(savingstransaction)
      .subscribe( data => {
        this.savingstransactions = this.savingstransactions.filter(st => st !== savingstransaction);
      });
  }
}
